import fs from 'fs';
import { resolve } from 'path';
import Jimp from 'jimp';
import mime from 'mime-types';

const { ipcMain } = require('electron');

export const resizeImages = async (path: string, size: string, doGrayscale: boolean) => {
  console.log('resize handler', path, size);
  const files = await fs.promises.readdir(path);

  const outDir = await fs.promises.mkdtemp(path);


  const [w, h] = size.split('x');
  const resizePromises = files.map(async (filePath) => {
    const fullPath = resolve(path, filePath);

    const stat = await fs.promises.stat(fullPath);
    if (!stat.isFile()) return;

    const mimetype = mime.lookup(fullPath);
    console.log(fullPath, mimetype);
    if (mimetype && !mimetype.startsWith('image/')) return;

    const outputPath = resolve(outDir, `resized_${filePath}`);

    console.log('resizing', fullPath, 'to', outputPath);

    const jimped = await Jimp.read(fullPath);
    jimped.resize(parseInt(w), parseInt(h));
    if (doGrayscale) jimped.grayscale();
    await jimped.writeAsync(outputPath);

    console.log(fullPath, jimped);
  });

  await Promise.all(resizePromises);
};

export const stopBuggingMe = 'about defautl exports';
