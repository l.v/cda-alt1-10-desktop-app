import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import icon from '../assets/icon.svg';
import './App.global.css';
import Select from 'react-select';
import { remote, ipcRenderer } from 'electron';

const { dialog } = remote;

type Option = { value: string, label: string};

const Hello = () => {
  const sizes: Option[] = [
    { value: '100x900', label: '100x900' },
    { value: '600x400', label: '600x400' },
    { value: '2x2', label: '2x2' },
  ];
  const [selectedOption, setSelectedOption] = useState<Option | null>(null);
  const [selectedPath, setPath] = useState<string | null>(null);

  const getFolder = async () => {
    const res = await dialog.showOpenDialog({
      properties: ['openDirectory'],
    });

    if (res.canceled) return;

    const path = res.filePaths[0];
    setPath(path);
  };

  const doResize = async () => {
    const promptRes = await dialog.showMessageBox({
      message: "t'es sur de ton coup ?",
      type: "question",
      buttons: ["vas y mon gars", "non attends"],
      checkboxLabel: "je vois la vie en gris",
    });

    if (promptRes.response !== 0) return;
    const doGrayscale = promptRes.checkboxChecked;

    const res = await ipcRenderer.invoke(
      'app:resize',
      selectedPath,
      selectedOption.value,
      doGrayscale,
    );
    console.log('done');
  };

  return (
    <div>
      <div className="Hello">
        <img width="200px" alt="icon" src={icon} />
      </div>
      <h1>retailleur d'images (fait à partir d'un plateau d'ébullition)</h1>
      <div className="Hello">
        <button type="button" onClick={getFolder}>
          <span role="img" aria-label="books">
            📚
          </span>
          pick a folder
        </button>

        <Select
          value={selectedOption}
          onChange={(option: string) => setSelectedOption(option)}
          options={sizes}
        />
      </div>
      <div>
        {selectedPath && selectedOption && (
          <button type="button" onClick={doResize}>
            resize images in {selectedPath}
          </button>
        )}
      </div>
    </div>
  );
};

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Hello} />
      </Switch>
    </Router>
  );
}
